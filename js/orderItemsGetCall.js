


var parsedJson;

var vegMains=[];
var nonVegMains=[];

var orderItems=[];

async function  login()
{


//window.location.href="orderItems.html";

    const resultPromise= await axios.get('http://localhost:8082/menu/all');
	
	console.log(resultPromise.data);


	parsedJson=resultPromise.data;

	console.log(parsedJson);

	var dishList=[];

	parsedJson.map((item)=>
	{		
		for(i=0;i<item.dishes.length;i++)
		{	//extracting dish object from individual dishes[] array
			dishList.push(item.dishes[i]);
		}
	})	

	



for(i=0;i<dishList.length;i++)
{


	
  //destructing JSON{} object into individual elements
  const {dishName,dishCode,cost,category}=dishList[i];
 

	if(category=="vegMains"||category=="vegStarters")
	{
	// creating chipDiv
	  var chipDiv=document.createElement("div");
	  chipDiv.setAttribute("class","chipDiv");
       	  chipDiv.setAttribute("id","div-"+i);
	  chipDiv.setAttribute("draggable",true);

	// creating input for checkbox
     	  var inputCheckBox = document.createElement("input");
    	  inputCheckBox.value =dishName +" : "+cost;
	  inputCheckBox.setAttribute("id",dishName);
    	  inputCheckBox.setAttribute("type","checkbox");
	  inputCheckBox.setAttribute("class","chipcheckBox");
	  inputCheckBox.addEventListener('click',menuItemSelected);
    	  inputCheckBox.text =dishName;
		
	//appending input to div
	chipDiv.appendChild(inputCheckBox);

	//creating label
	var label = document.createElement("label");
	label.setAttribute("class","chipLabel");		
    	label.innerHTML =dishName +" :  "+cost;
    	label.htmlFor = "vegCategories";
    	

	chipDiv.appendChild(label);


	//appending chipDiv to chipDivVegParentVegCategories
    	document.getElementById('chipDivParentVegCategories').appendChild(chipDiv);	 
    	

	}
	if(category=="nonVegMains"||category=="nonVegStarters")
	 {

		// creating chipDiv
	  var chipDiv=document.createElement("div");
	  chipDiv.setAttribute("class","chipDiv");
       	  chipDiv.setAttribute("id","div-"+i);

	// creating input for checkbox
     	  var inputCheckBox = document.createElement("input");
    	  inputCheckBox.value =dishName +" : "+cost;
	  inputCheckBox.setAttribute("id",dishName);
    	  inputCheckBox.setAttribute("type","checkbox");
	  inputCheckBox.setAttribute("class","chipcheckBox");
	  inputCheckBox.addEventListener('click',menuItemSelected);
    	  inputCheckBox.text =dishName;
	
	//appending input to div
	chipDiv.appendChild(inputCheckBox);
	
	
	//creating label
	var label = document.createElement("label");
	label.setAttribute("class","chipLabel");		
    	label.innerHTML =dishName +" : Rs "+cost;;
    	label.htmlFor = "nonVegCategories";
	

	//chipDiv appending labelDiv inside it
	chipDiv.appendChild(label);

	//appending chipDiv to chipDivVegParentVegCategories
    	document.getElementById('chipDivParentNonVegCategories').appendChild(chipDiv);	 

	
	}



}


}




function menuItemSelected(event) {

	var menuItemCheckBox=this;
	 
	var orderDiv=document.getElementById("orderDiv");

	
	var itemValue=menuItemCheckBox.value;

	 var splitItemAndPrice=itemValue.split(":");

	 var dishName=splitItemAndPrice[0];
	 var cost=splitItemAndPrice[1];


	if (menuItemCheckBox.checked == true){
   			
	  var orderItem=document.createElement("p");
	  orderItem.setAttribute("id",itemValue);
	  orderItem.setAttribute("class","orderItem");
	  orderItem.innerHTML=itemValue;	  
	  orderDiv.appendChild(orderItem);
	
	  var qtyItem=document.createElement("input");
	  qtyItem.setAttribute("type","number");
	  qtyItem.setAttribute("id","qty"+itemValue);
	  qtyItem.setAttribute("class","qtyItem");
	  qtyItem.setAttribute("min",1);
	  qtyItem.setAttribute("data-prev-value",1);
	  qtyItem.addEventListener("input",qtyItemFunction);
	  qtyItem.value=1;	  
		
	   orderDiv.appendChild(qtyItem);

	  var breakLine=document.createElement("br");
	  breakLine.setAttribute("id","breakLine"+itemValue);
	  breakLine.setAttribute("class","breakLine");
	  orderDiv.appendChild(breakLine);
	 
	
	  
	   var orderObject={"dishName":dishName,"cost":cost,"qty":1};
	
	   orderItems.push(orderObject);

	  var splitItemValue=itemValue.split(":");
	  var billValueElement=document.getElementById("billValue");
	
		var existingbill=billValueElement.innerHTML;	
	
		
		if(!isNaN(existingbill))
		{
			
	   		billValueElement.innerHTML=  parseInt(existingbill) +parseInt(splitItemValue[1]) ;
	


			if(parseInt(billValueElement.innerHTML) > 0)
			{	
				var sendOrderButton=document.getElementById("sendOrder");
				sendOrderButton.style.display = "inline-block";
							}
			if(parseInt(billValueElement.innerHTML) == 0)
			{
				var sendOrderButton=document.getElementById("sendOrder");
				sendOrderButton.style.display = "none";

			}


		}else
		{
			
	   		billValueElement.innerHTML= parseInt(splitItemValue[1]) ;	
		}

 	 }
	
	if(menuItemCheckBox.checked != true) 
	{
		
		var orderItem=document.getElementById(itemValue);
	
		var qtyItem=document.getElementById("qty"+itemValue);

 		var breakLine=document.getElementById("breakLine"+itemValue);
	
		var totalQtyOfItem=qtyItem.value;
		

		if(orderItem!="undefined")
		{
			orderDiv.removeChild(qtyItem);
	  		orderDiv.removeChild(orderItem);
	  		orderDiv.removeChild(breakLine);
			
		}

		  var splitItemValue=itemValue.split(":");

		  var billValueElement=document.getElementById("billValue");
	
		 var existingbill=billValueElement.innerHTML;	
	
		  if(parseInt(existingbill)!=0)
		  {
			
			billValueElement.innerHTML=  parseInt(existingbill) - parseInt(splitItemValue[1]) *totalQtyOfItem ;	
		  }

		if(parseInt(billValueElement.innerHTML) > 0)
			{	
				var sendOrderButton=document.getElementById("sendOrder");
				sendOrderButton.style.display = "inline-block";
							}
			if(parseInt(billValueElement.innerHTML) == 0)
			{
				var sendOrderButton=document.getElementById("sendOrder");
				sendOrderButton.style.display = "none";

			}


	
  	}

	 var orderDiv = document.getElementById("orderDiv");

	var childDiv = document.getElementById(itemValue);



	if(orderDiv.contains(childDiv))
	{
		var totalBill=document.getElementById("totalBill");
		

	}



}

function qtyItemFunction()
{
	var qtyItemId=this.id;
	// previousValue
	var previousValue=this.dataset.prevValue;
	//updatedValue
	var latestQtyValue=this.value;

	// splitting the qtyOrderItem
	 var splitItemValue=qtyItemId.split("qty");
	//extracting the orderITem
	 var orderItem=document.getElementById(splitItemValue[1]);	

	//extracting the orderValue of the item.
	var orderItemSplitItemValue=qtyItemId.split(":");


	//extracting dishName from the orderItem Element 
	var dishName=orderItem.innerHTML.split(":")[0];


	//latestQtyValue

	
	

	//orderObject

	// getting the billValueElement
	var billValueElement=document.getElementById("billValue");
	// getting the existing bill
	var existingbill=billValueElement.innerHTML;	

	if(parseInt(previousValue)<=parseInt(latestQtyValue))
	{
		billValueElement.innerHTML=parseInt(existingbill) +  parseInt(orderItemSplitItemValue[1]) ;
		
	}
	// it means new value is decremented Value
	if(parseInt(previousValue)>parseInt(latestQtyValue))
	{
		billValueElement.innerHTML=parseInt(existingbill) - parseInt(orderItemSplitItemValue[1]) ;		
	}

	//updating the previous value
	this.dataset.prevValue=latestQtyValue;

	for(item in orderItems)
	{
	
		//match the dishName and updated its qty value
		if(orderItems[item].dishName==dishName)
		{
			orderItems[item].qty=latestQtyValue;
			
		}
		
	}
		
	
}


async function sendOrder()
{

	var itemsToSend=JSON.stringify(orderItems);

	// create OrderId
	// getRestaurantCode
	// add Customer Name
	// add mobileNo;

	var order={"restaurantCode":"r1","dishes":orderItems};


	order=JSON.stringify(order);

	console.log(order);


	var a={"restaurantCode":"m1","dishes":[]};
	
	const resultLogin= await axios.post('http://localhost:8082/order',JSON.parse(order));

	console.log("Response "+resultLogin.data + resultLogin.status);
	
	var res=resultLogin.data

	



}



